﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{
	public int theDamage;
	bool isFoundHit = false;
	int raycastDistance = 1300;

	public Camera camera;

	public Text scoreText;
	public int currentScore;

	void Start ()
	{
		currentScore = 0;
		UpdateScore(currentScore);
	}

	void Update ()
	{
		RaycastHit hit = new RaycastHit ();
		DuckHealth healthComponent;

		if(Input.GetMouseButtonDown (0))
		{
			if (Physics.Raycast (transform.position, transform.forward, raycastDistance))
			{
				print ("Raycast hit detected.");
			}
			else
			{
				print ("No raycast hit detected.");
			}

			isFoundHit = Physics.Raycast(camera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0)), out hit, raycastDistance);

			if (isFoundHit) 
			{
				print ("Hit");
			}

			if (isFoundHit)
			{
				// gets a reference to the component!
				healthComponent = hit.collider.GetComponent<DuckHealth> ();

				// just for safety: Check that you actually found EnemyHealth component on the raycast hit, if not do not proceed
				if (healthComponent == null)
				{
					return; 
				}

				healthComponent.ApplyDamage (theDamage);

				currentScore += 100;
				UpdateScore (currentScore);
				print (currentScore);
				Debug.Log ("Working");
			}
		}
	}

	void UpdateScore(int currentScore)
	{
		scoreText.text = "Score: " + currentScore;
		currentScore = PlayerPrefs.GetInt("TOTALSCORE"); 
		PlayerPrefs.SetInt("SHOWSTARTSCORE",currentScore);
	}
} 
