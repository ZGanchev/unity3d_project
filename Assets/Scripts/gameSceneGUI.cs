﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSceneGUI : MonoBehaviour
{
	public Button GameExitButton;

	public void OnClick()
	{
		SceneManager.LoadScene ("main_menu_scene");
	}
}

