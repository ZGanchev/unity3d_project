﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour
{
    public void QuitLevelButton()
    {
        Application.Quit();
    }
}