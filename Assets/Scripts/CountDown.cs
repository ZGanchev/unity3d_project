﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class CountDown : MonoBehaviour
{
	float timeLeft = 360f;

	public Text timerText;
	public Text gameOverText;
	public Scene scene;

	private int currentGamerScore;

	void Update()
	{
		UpdateScreenTimer ();

		if (timeLeft < 0)
		{
			GlobalVariables.isGameOver = true;

			SceneManager.LoadScene ("main_menu_scene");

			// TODO: Show score when exit from the game.
			//currentGamerScore = GameObject.Find ("Characeter").GetComponent<GameController> ().currentScore;.....
		}
	}

	void UpdateScreenTimer()   
	{
		timeLeft -= Time.deltaTime;
		timerText.text = "Time Left: " + Mathf.Round(timeLeft);
	}
}


