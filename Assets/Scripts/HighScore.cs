﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScore : MonoBehaviour
{
	public Text scoreText;
	public Text highScoreText;

	private int currentScore;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		currentScore = GameObject.Find ("Character").GetComponent<GameController> ().currentScore;

		StoreHighscore (currentScore); 
		highScoreText.text = "Best: " + PlayerPrefs.GetInt ("highscore");
	}

	void StoreHighscore(int score)
	{
		int oldHighscore = PlayerPrefs.GetInt("highscore", 0);    

		if(score > oldHighscore)
		{
			PlayerPrefs.SetInt("highscore", score);
			highScoreText = scoreText;

			GlobalVariables.currentPlayerIsWinner = true;
		}
	}
}
