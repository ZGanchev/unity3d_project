﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour
{
	public Text gameOverText;
	public Text youWonText;

	void Start ()
	{
		if (GlobalVariables.isGameOver == true) 
		{
			gameOverText.text = "GAME  OVER!";

			if (GlobalVariables.currentPlayerIsWinner == true)
			{
				youWonText.text = string.Format ("Congratulations!\nYOU WON");
			}
		}
	}
} 
	