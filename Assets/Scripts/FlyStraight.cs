﻿using UnityEngine;
using System.Collections;

public class FlyStraight : MonoBehaviour {

    public float horizontalSpeed;
    private float verticalSpeed = 2f;
    private float amplithude = 0.5f;

    private Vector3 temporalPosition;

    // Use this for initialization
    void Start () {

        temporalPosition = transform.position;

    }
	
	// Update is called once per frame
	void Update () {

        temporalPosition.z += horizontalSpeed;
        temporalPosition.y += Mathf.Sin(Time.realtimeSinceStartup * verticalSpeed) * amplithude;
        transform.position = temporalPosition;

    }
}
