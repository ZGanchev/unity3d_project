﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour {

    public Canvas overlay;
    bool menu;

    void Start ()
    {
        menu = false;
        overlay.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("esc pressed");
            if (menu == true)
            {
                menu = false;
                overlay.enabled = false;
                Debug.Log("hiding menu");
            }
            else
            {
                menu = true;
                overlay.enabled = true;
                Debug.Log("showing menu");
            }
        }
	}
}