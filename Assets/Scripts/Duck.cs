﻿using UnityEngine;

public class Duck : MonoBehaviour 
{
	public GameObject duck;

	public GameObject duckPosition;

	public float horizontalSpeed;
	private float verticalSpeed = 2f;
	private float amplithude = 0.5f;

	public Animator controller;

	private Vector3 temporalPosition; 

	// Use this for initialization
	void Start () 
	{
		temporalPosition = duckPosition.transform.position;	

		controller = GetComponent<Animator> ();
		controller.SetBool ("Fly", true);
	}

	// Update is called once per frame
	void Update () 
	{
		temporalPosition.z += horizontalSpeed;
		temporalPosition.y += Mathf.Sin (Time.realtimeSinceStartup * verticalSpeed) * amplithude;
		transform.position = temporalPosition;
	}
}


