﻿using UnityEngine;
using System.Collections;

public class DuckHealth : MonoBehaviour
{
	private int health = 100;

	void Update()
	{
		if (health <= 0)
		{
			Debug.Log ("Duck Health is working");
			Dead ();
		}
	}

	public void ApplyDamage(int damageToTake)
	{
		damageToTake = 100;
		health -= damageToTake;
	}

	void Dead()
	{
		Destroy (gameObject);
	}
} 

